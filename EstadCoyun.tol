//////////////////////////////////////////////////////////////////////////////
// FILE: EstadCoyun.tol
// PURPOSE: Calendario de disponibilidad de estad�sticas coyunturales
//////////////////////////////////////////////////////////////////////////////

#Require PubDatIne;

Text PubDatInePath = GetAbsolutePath("./");

//////////////////////////////////////////////////////////////////////////////
Class @EncEstCoyDS 
//////////////////////////////////////////////////////////////////////////////
{
  // Informaci�n del paquete de datos:
  Text name; 
  Text desc;
  Text url;
  Real version_high;
  Real version_low;
  Real frozen;
  Text tms_base;
  Text tms_update;
  Text tms_advance;
  Set  add_urls;

  // Atributos auxiliares:
  Text _pkgName;
  TimeSet _tms_base;
  TimeSet _tms_update;
  TimeSet _tms_advance;

  Set token_tabla_set = [[
    "<TD class=\"tabla_inebase\">",
    "<p class=\"genpx_tit\">"
  ]];
  Text token_href_begin = "<a href=\"";
  Text token_href_end = "\"";

  //--------------------------------------------------------------------------
  // Constructores:

  ////////////////////////////////////////////////////////////////////////////
  // Constructor general con tres argumentos obligatorios y un n�mero
  // indefinido de argumetnos opcionales.
  Static @EncEstCoyDS Args(Text name_, Text desc_, Text url_, NameBlock args)
  ////////////////////////////////////////////////////////////////////////////
  {
    Set aux = [[ @EncEstCoyDS object = [[
      Text name = name_; 
      Text desc = desc_;
      Text url = url_;
      Real version_high = getOptArg(args, "version_high", 1);
      Real version_low = getOptArg(args, "version_low", 1);
      Real frozen = getOptArg(args, "frozen", False);
      Text tms_base = getOptArg(args, "base", "");
      Text tms_update = getOptArg(args, "update", "W");
      Text tms_advance = getOptArg(args, "advance", "W");
      Set add_urls = getOptArg(args, "add_urls", Copy(Empty));
      Text _pkgName = "PubDatIne"+name;
      TimeSet _tms_base = Eval(tms_base);
      TimeSet _tms_update = Eval(tms_update);
      TimeSet _tms_advance = Eval(tms_advance)
    ]] ]];
    NameBlock PutName(name_, aux[1]);
    NameBlock PutDescription(desc_, aux[1]);
    aux[1]
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @EncEstCoyDS New(
    Text name_, Text desc_, Text url_, Real frozen_,
    Text tms_base_, Text tms_update_, Text tms_advance_,
    Set add_urls_)
  ////////////////////////////////////////////////////////////////////////////
  {
    @EncEstCoyDS::Args(name_, desc_, url_, [[
      Real frozen = frozen_;
      Text tms_base = tms_base_;
      Text tms_update = tms_update_;
      Text tms_advance = tms_advance_;
      Set add_urls = add_urls_
    ]])
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @EncEstCoyDS Active(Text name_, Text desc_, Text url_, 
    Text tms_base_, Text tms_update_, Text tms_advance_, Set add_urls_)
  ////////////////////////////////////////////////////////////////////////////
  {
    New(name_,desc_,url_,False,tms_base_,tms_update_,tms_advance_,add_urls_)
  };

  ////////////////////////////////////////////////////////////////////////////
  Static @EncEstCoyDS Frozen(Text name_, Text desc_, Text url_, 
    Text tms_base_)
  ////////////////////////////////////////////////////////////////////////////
  {
    New(name_,desc_,url_,True,tms_base_,"W","W",Copy(Empty))
  };

  //--------------------------------------------------------------------------
  // M�todos:

  ////////////////////////////////////////////////////////////////////////////
  Real create_package(Real void)  
  ////////////////////////////////////////////////////////////////////////////
  {
    Real OSDirMake(PubDatInePath<<_pkgName);
    Text template = ReadFile(PubDatInePath<<"PubDatIneTemplate.tol");
    Text pkg_expr = ReplaceTable(template, [[
      [["##NAME##", name]],
      [["##DESC##", desc]],
      [["##URL##", url]],
      [["##VERSION_HIGH##", ""<<version_high]],
      [["##VERSION_LOW##", ""<<version_low]],
      [["##FROZEN##", ""<<frozen]],
      [["##TMS_BASE##", tms_base]],
      [["##TMS_UPDATE##", tms_update]],
      [["##TMS_ADVANCE##", tms_advance]]
    ]]);
    Text WriteFile(PubDatInePath<<_pkgName+"/"+_pkgName+".tol",pkg_expr);
    True
  };

  ////////////////////////////////////////////////////////////////////////////
  Real download(Real void)  
  ////////////////////////////////////////////////////////////////////////////
  {
    Real create_package(void);
    Real _download_url(url);
    Set EvalSet(add_urls, Real (Text add_url) { _download_url(add_url) });
    Set series = Select(_downloads, Card);
    Ois.Store(PutName(name, series), PubDatInePath<<_pkgName+"/series.oza")
  };

  ////////////////////////////////////////////////////////////////////////////
  Set _downloads = Copy(Empty);
  Real _download_url(Text _url)  
  ////////////////////////////////////////////////////////////////////////////
  {
    WriteLn("[#] Downloading URL: "<<_url);
    Case(!TextLength(_url), {
    0}, TextEndAt(_url,".px") | TextEndAt(_url,"down="), {
      Set Append(_downloads, [[PubDatIne::LoadSeriesFromPcaxis(_url,20)]]);
    1}, True, {
      Text contents = GetUrlContents(_url);
      Text contents := Replace(contents, "\n", " ");
      Text contents := Replace(contents, "  ", " ");
      Text contents := Replace(contents, "= ", "=");
      Text contents := Replace(contents, " =", "=");
      Text txt.1 = Replace(contents, "�", " ");
      Text txt.2 = txt.1;
      Set EvalSet(token_tabla_set, Real (Text token_tabla) {
        Text txt.2 := Replace(txt.2, token_tabla, "�");
      1});
      //Text txt.2 = Replace(txt.1, token_tabla, "�");
      Set tok = Tokenizer(txt.2,"�");
      Set aux.1 = For(2,Card(tok),Text(Real k)
      {
        //Real k = 2;
        Real pos.1 = TextFind(tok[k],token_href_begin);
        If(!pos.1,"",
        { 
          Real pos.ini = pos.1+TextLength(token_href_begin);
          Real pos.2 = TextFind(tok[k],token_href_end,pos.ini);
          If(!pos.2,"",
            Sub(tok[k],pos.ini,pos.2-1) )
        })
      });
      Set EvalSet(aux.1, Real(Text url_pcaxis)
      {
        Text url_base = TextSub(url, 1, 
          TextFind(url, "/", TextFind(url, ":/""/")+3)-1);
        Text url_ok = If(TextSub(url_pcaxis, 1, 1)=="/", url_base, "") 
          << url_pcaxis;
        _download_url(url_ok)
      });
    1})
  };

  ////////////////////////////////////////////////////////////////////////////
  Real upload_package(Real void)  
  ////////////////////////////////////////////////////////////////////////////
  {
    Real nErr0 = Copy(NError);
    Text path = PubDatInePath;
    // Se construye el paquete
    Text identifier = If(DirExist(path<<_pkgName), {
      Set package. = TolPackage::Packager::CompilePackage(_pkgName, path);
      Text zip = TolPackage::Packager::BuildZip(package.[1], path, 
        TolPackage::Builder::_.localRoot);
      Real nErr2 = Copy(NError);
      Text If(nErr2>nErr0, {
        WriteLn("El paquete '"<<_pkgName<<"' no se ha construido "
          <<"correctamente.", "E");
      ""}, {
        WriteLn("El paquete '"<<_pkgName<<"' se ha construido correctamente");
        // Se define el repositorio de paquetes:
        // La conexi�n "bysforofitol" debe haber sido creada y guardada 
        // anteriormente.
        NameBlock connection = StdLib::DBConnect::Obtain("bysforofitol");
        Real TolPackage::Builder::AppendRepository("Repos", connection);
        Text GetFilePrefix(zip)
      })
    }, {
      WriteLn("No se encuentra el paquete '"<<_pkgName<<"' en la ruta:\n  "
        <<path, "E");
    ""});
    Real nErr3 = Copy(NError);
    Real If(nErr3>nErr0, {
      WriteLn("El paquete no se ha construido correctamente", "E");
    0}, {
      Real TolPackage::Builder::UploadPackage(identifier, "Repos", 0);
      Real TolPackage::Builder::RemoveBuildedPackage(identifier);
    1});
  1}
};


//////////////////////////////////////////////////////////////////////////////
// Uso

Set EstadCoyun = [[
  @EncEstCoyDS::Args("CNTR_2008", 
    "Contabilidad Nacional Trimestral. Base 2008",
    "http://www.ine.es/jaxiBD/menu.do?L=0&divi=CNTR&his=2&type=db", [[
    Real version_high = 1;
    Real version_low = 2;
    Real frozen = False;
    Text base  = "Trimestral";
    Text update  = "M(2)*D(16)+M(5)*D(18)+M(8)*D(26)+M(11)*D(16)";
    Text advance = "M(2)*D(11)+M(5)*D(13)+M(8)*D(16)+M(11)*D(11)";
    Set add_urls = Copy(Empty)
  ]]);
  
  @EncEstCoyDS::Args("CNTR_2000",
    "Contabilidad Nacional Trimestral. Base 2000",
    "http://www.ine.es/jaxiBD/menu.do?L=0&divi=CNTR&his=0&type=db", [[
    Real version_high = 1;
    Real version_low = 2;
    Real frozen = True;
    Text base  = "Trimestral";
    Set add_urls = [[
      "http://www.ine.es/jaxiBD/menu.do?L=0&divi=CNTR&his=1&type=db"
    ]]
  ]]);
  
  @EncEstCoyDS::Args("CNTR_1995",
    "Contabilidad Nacional Trimestral. Base 1995",
    "http://www.ine.es/jaxiBD/menu.do?L=0&divi=CTA&his=0&type=db", [[
    Real version_high = 1;
    Real version_low = 1;
    Real frozen = True;
    Text base  = "Trimestral"
  ]]);
  
  @EncEstCoyDS::Args("EPA_2005_Principal",
    "Encuesta de Poblaci�n Activa (metodolog�a 2005).\n"
      "Principales resultados nacionales, auton�micos y provinciales.",
    "http://www.ine.es/jaxiBD/menu.do?L=0&divi=EPA&his=0&type=db", [[
    Real version_high = 1;
    Real version_low = 2;
    Text base  = "Trimestral";
    Real frozen = True;
    Text update  = "M(1)*D(28)+M(4)*D(29)+M(7)*D(29)+M(10)*D(28)";
    Text advance = "W"
  ]])
]];

//////////////////////////////////////////////////////////////////////////////

/*
Real (EstadCoyun[1])::download(?);
Real (EstadCoyun[2])::download(?);
Real (EstadCoyun[3])::download(?);
Real (EstadCoyun[4])::download(?);
*/
/*
Real (EstadCoyun[1])::upload_package(?);
Real (EstadCoyun[2])::upload_package(?);
-- Real (EstadCoyun[3])::upload_package(?);
Real (EstadCoyun[4])::upload_package(?);
*/

//////////////////////////////////////////////////////////////////////////////

